/*
東京電機大学理工学部後期講義課題
TokyoDenkiUniversity Department of Science and Engineering
Information coding theory assignment

当コードの如何なる転載、改変を禁じる
I do not approve of reprint and modification of this code

CopyRight 2017 15rd145 ryo
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT_BIT_LENGTH 4
#define BIT_LENGTH 7
#define CHECK_ARRAY_X 7
#define CHECK_ARRAY_Y 4

#define TRUE 1
#define FALSE 0


void Input(char input[], int inoutBIT[INPUT_BIT_LENGTH]){
	char *tok;

	for (int i = 0; i < INPUT_BIT_LENGTH; i++){
		if (i == 0)
			tok = strtok(input, ",");
		else
			tok = strtok(NULL, ",");

		if (tok == NULL){
			printf("InputError\n");
			exit(1);
		}
		inoutBIT[i] = atoi(tok);
	}
}

void generateBit(int inputBit[BIT_LENGTH] , int checkarray[CHECK_ARRAY_Y][CHECK_ARRAY_X]){
	int s[BIT_LENGTH] = { {0} };

	for (int i = 0; i < BIT_LENGTH;i++){
		for (int n = 0; n < INPUT_BIT_LENGTH; n++){
			s[i] += inputBit[n] * checkarray[n][i];
		}
		s[i] %= 2;
	}
	for (int i = 0; i < BIT_LENGTH; i++){ inputBit[i] = s[i]; }
	return;
}

int main(){
	char input[100];
	int inputBit[BIT_LENGTH];

	int checkarray[CHECK_ARRAY_Y][CHECK_ARRAY_X] = {
		1,0,0,0,1,0,1,
		0,1,0,0,1,1,1,
		0,0,1,0,1,1,0,
		0,0,0,1,0,1,1
	};

	printf("Plaese Bit x1,x2,x3,x4,\n");
	scanf("%s", &input);

	Input(input,inputBit);

	generateBit(inputBit,checkarray);

	printf("\nResult\n");
	for (int i = 0; i < BIT_LENGTH;i++)printf("%d,", inputBit[i]);

	return 0;
}