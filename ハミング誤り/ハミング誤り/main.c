/*
東京電機大学理工学部後期講義課題
TokyoDenkiUniversity Department of Science and Engineering
Information coding theory assignment

当コードの如何なる転載、改変を禁じる
I do not approve of reprint and modification of this code

CopyRight 2017 15rd145 ryo
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BIT_LENGTH 7
#define CHECK_ARRAY_X 7
#define CHECK_ARRAY_Y 3

#define DECODARRAY CHECK_ARRAY_Y

#define TRUE 1
#define FALSE 0


/*データ入力部*/
void Input(char input[] , int inoutBIT[BIT_LENGTH]){
	char *tok;

	for (int i = 0; i < BIT_LENGTH; i++){
		if (i == 0)
			tok = strtok(input, ",");
		else
			tok = strtok(NULL, ",");

		if (tok == NULL){
			printf("InputError\n");
			exit(1);
		}
		inoutBIT[i] = atoi(tok);
	}
}


void CheckTF( int output[DECODARRAY] , int checkarray[CHECK_ARRAY_Y][CHECK_ARRAY_X],int AncerBIT[BIT_LENGTH]){
	int s = 0;
	for (int i = 0; i < DECODARRAY; i++)s += output[i];

	if (s == 0){ 
		printf("This Array is TRUE"); 
		return;
	}

	printf("This Array is FALSE\n");


	for (int n = 0; n < CHECK_ARRAY_X; n++){
		s = CHECK_ARRAY_Y;;
		for (int i = 0; i < DECODARRAY; i++){
			if (output[i] == checkarray[i][n])s--;
		}
		if (s == 0){
			AncerBIT[n] += 1;
			AncerBIT[n] %= 2;
			printf("FalseBit is %d bit(0~%d)", n, BIT_LENGTH - 1);
			break;
		}
	}

	printf("\n\nAncerBit: ");
	for (int i = 0; i < BIT_LENGTH;i++){
		printf("%d",AncerBIT[i]);
		if (i != BIT_LENGTH - 1)printf(",");
	}

	return;
}



void Decoding(int inputBIT[BIT_LENGTH], int checkBIT[CHECK_ARRAY_Y][CHECK_ARRAY_X], int output[DECODARRAY]){
	int s;
	for (int y = 0; y < CHECK_ARRAY_Y; y++){
		output[y] = 0;
		for (int x = 0; x < CHECK_ARRAY_X; x++){
			output[y] += inputBIT[x] * checkBIT[y][x];
		}
		output[y] %= 2;
	}
	return;
}


int main(void){
	char input[100];
	int inputBit[BIT_LENGTH];

	int output[DECODARRAY];

	int checkarray[CHECK_ARRAY_Y][CHECK_ARRAY_X] = {
		1, 1, 1, 0, 1, 0, 0,
		0, 1, 1, 1, 0, 1, 0,
		1, 1, 0, 1, 0, 0, 1 };

	printf("Plaese Haming Num\n");
	printf("x1,x2,x3,x4,c1,c2,c3,\nx:InformationBit\nc:CheckBit\n\n");
	scanf("%s",&input);

	Input(input,inputBit);
	Decoding(inputBit,checkarray,output);
	CheckTF(output,checkarray,inputBit);

	return 0;
}